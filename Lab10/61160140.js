/*
http://127.0.0.1:3000/ to connect database
http://127.0.0.1:3000/getAll to Select all data
http://127.0.0.1:3000/form to add data
http://127.0.0.1:3000/update to update city on id:1
http://127.0.0.1:3000/delete/:id to delete data on {id}
http://127.0.0.1:3000/get/:id to show data on {id}
*/
const express = require('express')
const mysql = require('mysql')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
const app = express()
app.use(bodyParser.json())
app.use(morgan('dev'))
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
const con = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'sitepoint'
})

// connect
app.get('/', function (req, res, next) {
  con.connect((err) => {
    if (err) {
      console.log('Error connecting to Db ' + err)
      res.send('Error connecting to Db ' + err)
      return
    }
    console.log('Connection established')
    res.send('Connection established')
  })
})

// get all
app.get('/getAll', function (req, res, next) {
  res.set({ 'Content-Type': 'text/plain; charset=utf-8' })
  con.query('SELECT * FROM authors', (err, rows) => {
    if (err) throw err
    // console.log('Data received from Db:')
    // console.log(rows)
    rows.forEach((row) => {
      console.log(`${row.name} lives in ${row.city}`)
      res.write(`${row.name} lives in ${row.city} \n`)
    })
    res.end()
  })
})

// Insert
app.get('/form', function (req, res, next) {
  res.sendFile(__dirname + "/html/" + "form.html")
})
app.post('/add', function (req, res, next) {
  var author = { name: req.body.name, city: req.body.city }
  let insert = 0
  con.query('INSERT INTO authors SET ?', author, (err, res2) => {
    if (err) throw err
    console.log('Last insert ID:', res2.insertId)
    insert = res2.insertId
    return res.status(201).json({
      code: 1,
      message: 'OK',
      LastId: insert
    });
  })
})

// Update
app.get('/update', function (req, res, next) {
  con.query('UPDATE authors SET city = ? Where name = ?', ['Khonkan', 'Michaela Lehr'], (err, result) => {
    if (err) throw err;
    console.log(`Changed ${result.changedRows} row(s)`)
  })
  con.query('SELECT * FROM authors Where name = ?', 'Michaela Lehr', (err, result) => {
    if (err) throw err
    res.send(result)
  })
})

// Delete
app.get('/delete/:id', function (req, res, next) {
  const id = req.params.id
  res.set({ 'Content-Type': 'application/JSON' })
  con.query('DELETE FROM authors WHERE id = ?', id, (err, result) => {
    if (err) throw err;
    console.log(`Deleted ${result.affectedRows} row(s)`)
  })
  con.query('SELECT * FROM authors', (err, rows) => {
    if (err) throw err
    // console.log('Data received from Db:')
    console.log(rows)
    res.send(rows)
  })
})

// Select by id
app.get('/get/:id', function (req, res, next) {
  const id = req.params.id
  con.query('SELECT * FROM authors Where ID = ?', id, (err, result) => {
    if (err) throw err
    console.log(result)
    res.send(result)
  })
})

app.listen(3000, function () {
  console.log('Listen on server 127.0.0.1:3000')
})

