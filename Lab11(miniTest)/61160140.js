const express = require('express')
const mysql = require('mysql')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
const alert = require('alert')
const app = express()
app.use(bodyParser.json())
app.use(morgan('dev'))
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.engine('html', require('ejs').renderFile)
app.set('view engine', 'html')
const con = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'dcoffee'
})

con.connect((err) => {
  if (err) {
    console.log('Error connecting to Db ' + err)
    res.send('Error connecting to Db ' + err)
    return
  }
  console.log('Connection established')
})
app.listen(3000, function () {
  console.log('Listen on server 127.0.0.1:3000')
})

app.get('/', function (req, res, next) {
  res.sendFile(__dirname + "/html/" + "login.html")
})

// declare variable
let name
let surname
let id
let position
let salary
let total_sale
let login
let count
let logout
function formatDate(date) {
  return date.toISOString().split('T')[0] + ' ' + date.toTimeString().split(' ')[0];
}

app.post('/check', function (req, res, next) {
  user = req.body.email
  password = req.body.password
  con.query('SELECT mst_employee.name, mst_employee.surname, mst_employee.id_employee, mst_employee.position, mst_employee.salary, mst_employee.total_sale, trn_logout.datetime_logout FROM mst_security INNER JOIN mst_employee ON mst_security.id_employee = mst_employee.id_employee INNER JOIN trn_logout ON mst_security.id_employee = trn_logout.id_employee WHERE USER = ? AND PASSWORD = ?', [user, password], (err, result) => {
    if (err) throw err

    if (result.length > 0) {
      name = result[result.length - 1].name
      surname = result[result.length - 1].surname
      id = result[result.length - 1].id_employee
      position = result[result.length - 1].position
      salary = commaSeparateNumber(result[result.length - 1].salary)
      total_sale = commaSeparateNumber(result[result.length - 1].total_sale)
      logout = formatDate(result[result.length - 1].datetime_logout)

      function commaSeparateNumber(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
          val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
        }
        return val;
      }

      con.query('SELECT COUNT(trn_login.id_login) AS countLogin FROM trn_login INNER JOIN mst_security ON trn_login.id_employee = mst_security.id_employee WHERE mst_security.USER = ? AND mst_security.PASSWORD = ?', [user, password], (err, result2) => {
        if (err) throw err
        count = result2[result2.length - 1].countLogin

        con.query('SELECT datetime_login FROM trn_login INNER JOIN mst_security ON trn_login.id_employee = mst_security.id_employee WHERE mst_security.USER = ? AND mst_security.PASSWORD = ?', [user, password], (err, result3) => {
          if (err) throw err
          login = formatDate(result3[result3.length - 1].datetime_login)
          let date_ob = new Date()
          var loginTime = { datetime_login: formatDate(date_ob), id_employee: id }

          con.query('INSERT INTO trn_login SET ?', loginTime, (err, res2) => {
            if (err) throw err
            console.log('Last insert ID:', res2.insertId)
            res.render(__dirname + '/html/member.html',
              {
                name: name,
                surname: surname,
                id: id,
                position: position,
                salary: salary,
                total_sale: total_sale,
                login: login,
                logout: logout,
                count: count
              })
          })
        })
      })

    } else {
      alert("คุณไม่เป็นสมาชิก")
      res.sendFile(__dirname + "/html/" + "login.html")
    }
  })
})

app.get('/logout', function (req, res, next) {
  let date_ob = new Date()
  var logoutTime = { datetime_logout: formatDate(date_ob), id_employee: id }
  con.query('INSERT INTO trn_logout SET ?', logoutTime, (err, res) => {
    if (err) throw err
    console.log('Last insert ID:', res.insertId)
  })
  res.sendFile(__dirname + "/html/" + "login.html")
})