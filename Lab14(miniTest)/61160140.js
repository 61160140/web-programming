const express = require('express')
const mysql = require('mysql')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
const alert = require('alert')
const app = express()
app.use(bodyParser.json())
app.use(morgan('dev'))
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.engine('html', require('ejs').renderFile)
app.set('view engine', 'html')
app.use(express.static(__dirname))
app.use('/images', express.static(__dirname + '/img'))
const con = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'dcoffee'
})

con.connect(err => {
  if (err) {
    console.log('Error connecting to Db ' + err)
    res.send('Error connecting to Db ' + err)
    return
  }
  console.log('Connection established')
})
app.listen(3000, function () {
  console.log('Listen on server 127.0.0.1:3000')
})

app.get('/', function (req, res, next) {
  res.sendFile(__dirname + '/html/' + 'login.html')
})

// declare variable
let name
let surname
let id
let position
let salary
let total_sale
let login
let count
let logout

function formatDate (date) {
  return (
    date.toISOString().split('T')[0] + ' ' + date.toTimeString().split(' ')[0]
  )
}

app.post('/check', function (req, res, next) {
  user = req.body.email
  password = req.body.password
  console.log(user + ' ' + password)
  con.query(
    'SELECT mst_employee.name, mst_employee.surname, mst_employee.id_employee, mst_employee.position, FORMAT(mst_employee.salary,2) AS salary , FORMAT(mst_employee.total_sale,2) as total_sale FROM mst_security INNER JOIN mst_employee ON mst_security.id_employee = mst_employee.id_employee WHERE USER = ? AND PASSWORD = ?',
    [user, password],
    (err, result) => {
      if (err) throw err
      if (result.length > 0) {
        name = result[result.length - 1].name
        surname = result[result.length - 1].surname
        id = result[result.length - 1].id_employee
        position = result[result.length - 1].position
        salary = result[result.length - 1].salary
        total_sale = result[result.length - 1].total_sale
        con.query(
          'SELECT datetime_logout FROM trn_logout INNER JOIN mst_security ON trn_logout.id_employee = mst_security.id_employee WHERE USER = ? AND PASSWORD = ?',
          [user, password],
          (err, result) => {
            if (err) {
              throw err
            }
            if (result.length > 0) {
              logout = formatDate(result[result.length - 1].datetime_logout)
            }
          }
        )

        con.query(
          'SELECT COUNT(trn_login.id_login) AS countLogin FROM trn_login INNER JOIN mst_security ON trn_login.id_employee = mst_security.id_employee WHERE mst_security.USER = ? AND mst_security.PASSWORD = ?',
          [user, password],
          (err, result2) => {
            if (err) throw err
            if (result2.length > 0) {
              count = result2[result2.length - 1].countLogin

              con.query(
                'SELECT datetime_login FROM trn_login INNER JOIN mst_security ON trn_login.id_employee = mst_security.id_employee WHERE mst_security.USER = ? AND mst_security.PASSWORD = ?',
                [user, password],
                (err, result3) => {
                  if (err) throw err
                  if (result3.length > 0) {
                    login = formatDate(
                      result3[result3.length - 1].datetime_login
                    )
                  }
                }
              )
            }

            let date_ob = new Date()
            var loginTime = {
              datetime_login: formatDate(date_ob),
              id_employee: id
            }
            con.query('INSERT INTO trn_login SET ?', loginTime, (err, res2) => {
              if (err) throw err
              console.log('Last insert ID:', res2.insertId)
            })
          }
        )

        if (position === 'เจ้าของร้าน') {
          res.redirect('/main')
        } else {
          res.redirect('/main_emp')
        }
      } else {
        alert('คุณไม่เป็นสมาชิก')
        res.redirect('/')
      }
    }
  )
})

app.get('/logout', function (req, res, next) {
  let date_ob = new Date()
  var logoutTime = { datetime_logout: formatDate(date_ob), id_employee: id }
  con.query('INSERT INTO trn_logout SET ?', logoutTime, (err, res) => {
    if (err) throw err
    console.log('Last insert ID:', res.insertId)
  })
  name = ''
  surname = ''
  id = ''
  position = ''
  salary = ''
  total_sale = ''
  login = ''
  logout = ''
  res.redirect('/')
})

app.get('/main', function (req, res, next) {
  res.render(__dirname + '/html/main.html', {
    name: name,
    surname: surname
  })
})

app.get('/main_emp', function (req, res, next) {
  res.render(__dirname + '/html/main_emp.html', {
    name: name,
    surname: surname
  })
})

app.get('/member.html', function (req, res, next) {
  res.render(__dirname + '/html/member.html', {
    name: name,
    surname: surname,
    id: id,
    position: position,
    salary: salary,
    total_sale: total_sale,
    login: login,
    logout: logout,
    count: count
  })
})

app.get('/memberControl.html', function (req, res, next) {
  con.query(
    'SELECT id_employee, name, surname, position, FORMAT(salary,2) AS salary , FORMAT(total_sale,2) as total_sale FROM `mst_employee`',
    (err, data) => {
      if (err) throw err
      console.log(data)
      res.render(__dirname + '/html/memberControl.html', { data: data })
    }
  )
})

app.get('/addmember.html', function (req, res, next) {
  res.sendFile(__dirname + '/html/' + 'addmember.html')
})

app.post('/addMember', function (req, res, next) {
  var employee = {
    name: req.body.name,
    surname: req.body.surname,
    position: req.body.position,
    salary: req.body.salary,
    total_sale: req.body.total_sale
  }

  con.query('INSERT INTO mst_employee SET ?', employee, (err, res2) => {
    if (err) throw err
    console.log('Last insert ID:', res2.insertId)
    insert = res2.insertId
    var security = {
      user: req.body.email,
      password: req.body.password,
      id_employee: res2.insertId
    }
    con.query('INSERT INTO mst_security SET ?', security, (err, res3) => {
      if (err) throw err
      console.log('Last insert ID:', res3.insertId)
      return res.status(201)
    })
  })
  res.redirect('/main')
})

app.get('/search/:word', function (req, res, next) {
  let { word } = req.params
  search = "'%"
  search += word
  search += "%'"
  console.log(search)
  let query =
    'SELECT id_employee, name, surname, position, salary , total_sale FROM mst_employee WHERE name LIKE ' +
    search +
    ' OR surname LIKE ' +
    search +
    ' OR position LIKE ' +
    search
  con.query(query, (err, data) => {
    if (err) throw err
    res.render(__dirname + '/html/memberControl.html', { data: data })
  })
})

app.get('/edit/:id', function (req, res, next) {
  let { id } = req.params
  con.query(
    'SELECT name, surname, position, salary, total_sale, user ,password FROM mst_employee INNER JOIN mst_security ON mst_security.id_employee = mst_employee.id_employee WHERE mst_employee.id_employee = ?',
    id,
    (err, result) => {
      if (err) throw err
      if (result.length > 0) {
        name2 = result[result.length - 1].name
        surname2 = result[result.length - 1].surname
        position2 = result[result.length - 1].position
        salary2 = result[result.length - 1].salary
        total_sale2 = result[result.length - 1].total_sale
        user2 = result[result.length - 1].user
        password2 = result[result.length - 1].password
      }
      res.render(__dirname + '/html/editmember.html', {
        name: name2,
        surname: surname2,
        id: id,
        position: position2,
        salary: salary2,
        total_sale: total_sale2,
        user: user2,
        password: password2
      })
    }
  )
})

app.post('/editMember/:id', function (req, res, next) {
  let { id } = req.params
  var employee = {
    name: req.body.name,
    surname: req.body.surname,
    position: req.body.position,
    salary: req.body.salary,
    total_sale: req.body.total_sale
  }

  con.query(
    'UPDATE mst_employee SET ? WHERE id_employee = ?',
    [employee, id],
    (err, res2) => {
      if (err) throw err
      var security = {
        user: req.body.email,
        password: req.body.password
      }
      con.query(
        'UPDATE mst_security SET ? WHERE id_employee = ?',
        [security, id],
        (err, res3) => {
          if (err) throw err
          return res.status(200)
        }
      )
    }
  )
  res.redirect('/main')
})

app.get('/delete/:id', function (req, res, next) {
  let { id } = req.params
  con.query(
    'SELECT name, surname, position FROM mst_employee WHERE id_employee = ?',
    id,
    (err, result) => {
      if (err) throw err
      if (result.length > 0) {
        name2 = result[result.length - 1].name
        surname2 = result[result.length - 1].surname
        position2 = result[result.length - 1].position
      }
      res.render(__dirname + '/html/deletemember.html', {
        name: name2,
        surname: surname2,
        id: id,
        position: position2,
      })
    }
  )
})

app.get('/deleteMember/:id', function (req, res, next) {
  let { id } = req.params
  con.query('DELETE FROM trn_logout WHERE id_employee = ?',id,(err, result) => {
    if (err) throw err
    console.log(`Deleted ${result.affectedRows} row(s)`)
  })
  con.query('DELETE FROM trn_login WHERE id_employee = ?',id,(err, result) => {
      if (err) throw err
      console.log(`Deleted ${result.affectedRows} row(s)`)
  })
  con.query('DELETE FROM mst_security WHERE id_employee = ?',id,(err, result) => {
    if (err) throw err
    console.log(`Deleted ${result.affectedRows} row(s)`)
  })
  con.query('DELETE FROM mst_employee WHERE id_employee = ?',id,(err, result) => {
    if (err) throw err
    console.log(`Deleted ${result.affectedRows} row(s)`)
  })
  res.redirect('/main')
})
