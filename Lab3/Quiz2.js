var http = require('http')
http
  .createServer(function (req, res) {
    var path = req.url.replace(/\/?(?:\?.*)?$/, '').toLowerCase()
    switch (path) {
      case '':
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end('Student Name')
        break
      case '/1':
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end('Student Address')
        break
      case '/2':
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end('Student Name + Address')
        break
      case '/3':
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end('Student Name + Address + a good friend name')
        break
      default:
        res.writeHead(200, { 'Content-Type': 'text/plain' })
        res.end('I am a good Programmer')
        break
    }
  })
  .listen(3000, '127.0.0.1')
console.log('Server started on localhost:3000')
