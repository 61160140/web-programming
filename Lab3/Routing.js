var http = require('http')
http
  .createServer(function (req, res) {
    var path = req.url.replace(/\/?(?:\?.*)?$/, '').toLowerCase()
    switch (path) {
      case '':
        res.writeHead(200, { 'Conten-Type': 'text/plain' })
        res.end('Homepage')
        break
    }
  })
  .listen(3000, '127.0.0.1')
console.log('Server started on localhost:3000')
