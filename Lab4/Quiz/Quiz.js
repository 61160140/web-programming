var express = require('express')
var app = express()
app.get('/', function (req, res) {
  console.log('Got a GET request for the homepage')
  res.send('Nutchaya Pintabut')
})

app.get('/1', function (req, res) {
  console.log('Got a GET request for /1')
  res.send('242 Pibulsongkram Rd. Muang,Nonthaburi')
})

app.get('/2', function (req, res) {
  console.log('Got a GET request for /2')
  res.send('Nutchaya Pintabut + 242 Pibulsongkram Rd. Muang,Nonthaburi')
})

app.get('/3', function (req, res) {
  console.log('Got a GET request for /3')
  res.send('Nutchaya Pintabut + 242 Pibulsongkram Rd. Muang,Nonthaburi + Gib')
})

var server = app.listen(8081, function () {
  var port = server.address().port
  console.log('The port number of Server : %s', port)
})
