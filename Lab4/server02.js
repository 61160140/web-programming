var express = require('express')
var app = express()
app.get('/', function (req, res) {
  console.log('Got a GET request for the homepage')
  res.send('Hello GET')
})

app.get('/aaa', function (req, res) {
  console.log('Got a GET request for /aaa')
  res.send('Page Pattern Match')
})

app.get('/list_user', function (req, res) {
  console.log('Got a GET request for /list_user')
  res.send('Page Listing')
})

var server = app.listen(8081, function () {
  var port = server.address().port
  console.log('The port number of Server : %s', port)
})
