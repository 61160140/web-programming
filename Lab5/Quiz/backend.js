var express = require('express')
var app = express()
app.use(express.static('public'))
app.get('/index.html', function (req,res){
  res.sendFile(_dirname+"/"+"index.html")
})
app.get('/process_get', function (req,res){
  var response = {
    first_name:req.query.first_name,
    last_name:req.query.last_name,
    gender:req.query.gender,
    birth_date:req.query.birth_date,
    address:req.query.address
  }
  console.log(response)
  res.end(JSON.stringify(response))
})
app.listen(8081)