var express = require('express')
var app = express()
var fs = require('fs')

app.use(express.static('public'))
app.get('/', function (req, res) {
  res.send('Welcome to CS page')
})
app.get('/about', function (req, res) {
  res.sendFile(__dirname + '/public' + "/html/" + "student.html")
})
app.get('/img', function (req, res) {
  res.sendFile(__dirname + "/public" + "/image/" + "7-Eleven-logo.png")
})
app.get('/login', function (req, res) {
  res.sendFile(__dirname + '/public' + "/html/" + "login.html")
})
app.get('/process_get', function (req, res) {
  var response = {
    user_name: req.query.user_name,
    password_name: req.query.password_name
  }
  res.end(JSON.stringify(response))
})
app.get('/syn', function (req, res) {
  var data = fs.readFileSync('./public/file/information.txt')
  console.log(data.toString())
  console.log('*****Synchronous****')
})
app.get('/asyn', function (req, res) {
  var data = fs.readFile('./public/file/information.txt', function (err, data) {
    console.log(data.toString())
  })
  console.log('*****Asynchronous****')
})

app.listen(3000)
