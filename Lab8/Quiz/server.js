const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
let users = require('./user.json')
let app = express()
app.use(bodyParser.json())
app.use(morgan('dev'))
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.listen(3000, function () {
  console.log('User : ', users)
})
//app.get all
app.get('/user/all', function (req, res, next) {
  return res.status(200).json({
    code: 1,
    message: 'OK',
    data: users
  })
})
//app.get id
app.get('/user/show/:id', function (req, res, next) {
  const userId = req.params.id
  const user = users.find(_user => _user.id.toString() === userId)
  return res.status(200).json({
    code: 1,
    message: 'OK',
    data: user
  })
})