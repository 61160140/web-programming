const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
let users = require('./user.json')
let app = express()
app.use(bodyParser.json())
app.use(morgan('dev'))
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static('public'))
app.listen(3000, function () {
  console.log('User : ', users)
})

//app.get all
app.get('/user/all', function (req, res, next) {
  return res.status(200).json({
    code: 1,
    message: 'OK',
    data: users
  })
})

//app.get id
app.get('/user/show/:id', function (req, res, next) {
  const userId = req.params.id
  const position = users.findIndex(function (val) {
    return val.id.toString() === userId
  })
  // const user = users.find(_user => _user.id.toString() === userId)
  return res.status(200).json({
    code: 1,
    message: 'OK',
    data: users[position]
  })
})

//app.del id
app.get('/user/del/:id', function (req, res, next) {
  const removeId = req.params.id
  const position = users.findIndex(function (val) {
    return val.id.toString() === removeId
  })
  users.splice(position, 1)
  return res.status(200).json({
    code: 1,
    message: 'OK',
    data: users
  })

})

// app.post
app.get('/form', function (req, res, next) {
  res.sendFile(__dirname + '/public' + "/html/" + "form.html")
})
app.post('/user/add', function (req, res, next) {
  var user = {}
  user.id = users.length + 1
  user.name = req.body.name
  user.age = Number(req.body.age)
  user.movie = req.body.movie
  users.push(user)
  console.log('Users :', user.name, 'Created!')
  return res.status(201).json({
    code: 1,
    message: 'OK',
    data: users
  });
})

// app.edit
app.get('/formEdit', function (req, res, next) {
  res.sendFile(__dirname + '/public' + "/html/" + "formEdit.html")
})
app.post('/user/edit',function(req,res,next){
  const replaceId = req.body.id
  console.log(req.body.id)
  const position = users.findIndex(function (val) {
    return val.id.toString() === replaceId
  })
  console.log(position)
  users[position].name = req.body.name
  users[position].age = Number(req.body.age)
  users[position].movie = req.body.movie
  return res.status(200).json({
    code: 1,
    message: 'OK',
    data: users
  })
})