// ejs
const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')
let users = require('./user.json')
let app = express()
app.use(bodyParser.json())
app.use(morgan('dev'))
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static('public'))
app.set('views', './public/views');
app.set('view engine', 'ejs');

app.listen(3000, function () {
  console.log('User : ', users)
})

app.get('/', function (req, res, next) {
  res.send('Plese go to http://127.0.0.1:3000/login')
})

app.get('/login', function (req, res, next) {
  res.sendFile(__dirname + '/public' + "/html/" + "login.html")
})

app.post('/check', function (req, res, next) {
  var user = {}
  user.email = req.body.email
  user.password = req.body.password
  // console.log(user.email)
  // console.log(user.password)
  console.log(req.body.email)
  console.log(req.body.password)
  if (req.body.email == '' || req.body.password == '') {
    res.send('Please fill the form')
  } else {
    const position = users.findIndex(function (val) {
      if (val.email === user.email) {
        return val.password === user.password
      }
    })
    console.log(position)
    if (users[position].member === 'Yes') {
      // app.get('/member', function (req, res, next){
      res.render('member',
        {
          name: users[position].name,
          age: users[position].age,
          movie: users[position].movie,
          email: users[position].email
        })
      // })
    } else {
      // app.get('/nonmember', function (req, res, next){
      res.sendFile(__dirname + '/public' + "/html/" + "nonmember.html")
      // })
    }
  }

})
